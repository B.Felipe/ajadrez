import doctest

"""
Va a testear todo lo que este dentro de las tres comillas
mas especifico lo que esta señalado con >>>
y abajo de eso, es lo que se espera que se vuelva

"""

a = 3
b = 2
c = 6
d = 0

def multiplicacion(a,b) -> int:


        """

        >>> a*b
        6
        
        """

        return multiplicacion
doctest.testmod(name='multiplicacion', verbose=True)

def division (c,d) -> int:

        """
        >>> c/d
        3
        """
        return division

doctest.testmod(name='divison', verbose=True)